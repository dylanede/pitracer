#ifndef SHAPE_H
#define SHAPE_H

#include "Ray.h"
#include <cstring>

template<typename Real>
class Shape
{
public:

    typedef struct Intersection{
        Vector3<Real> position, normal;
        size_t material_id;
        Real d;
        bool hit;
        inline Intersection() : hit(false) {}
        inline Intersection(const Vector3<Real>& position, const Vector3<Real>& normal, const Real& d, size_t material_id) : hit(true), position(position), normal(normal), d(d), material_id(material_id) {}
    } Intersection;

    virtual Intersection intersect(const Ray<Real>& ray) const = 0;
    virtual Intersection intersect(const Ray<Real>& ray, Real distance) const = 0;
    virtual bool intersects(const Ray<Real>& ray) const = 0;
    virtual bool intersects(const Ray<Real>& ray, Real distance) = 0;

    virtual ~Shape() {}
};

#endif // SHAPE_H
