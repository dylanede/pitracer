#ifndef MATERIAL_H
#define MATERIAL_H

#include "Vector3.h"

#include "Tracer.h"

template<typename Real, typename Result, typename Location>
class Material {
public:
    struct NodeList {
        Vector3<Real>* node;
        size_t count;
        virtual ~NodeList();
    };

    virtual Result sample(const Ray<Real>& ray, const Vector3<Real>& position, const Vector3<Real>& normal, Tracer<Real, Result, Location>& tracer) const = 0;

    //template<typename Result>
    //virtual NodeList generate()
    //virtual Result sample(const Ray<Real>& ray, const Vector3<Real>& position, const Vector3<Real>& normal, Real* parms);

    //virtual size_t parmCount();

    virtual ~Material() {}
};

#endif // MATERIAL_H
