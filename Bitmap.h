#ifndef BITMAP_H
#define BITMAP_H

#include <cstdlib>
#include <functional>

template <typename Value>
class Bitmap {
    size_t width, height;
    size_t stride;
public:
    Value* values;

    Bitmap(size_t width, size_t height)
        : width(width), height(height), stride(width*sizeof(Value)), values(new Value[height * width]) {
    }

    template<typename T>
    Bitmap(const Bitmap<T>& b) : width(b.getWidth()), height(b.getHeight()), stride(width*sizeof(Value)), values(new Value[height * width]) {
        for(size_t y = 0, i = 0; y < height; ++y)
            for(size_t x = 0; x < width; ++x, ++i)
                values[i] = b.values[i];
    }
    Bitmap& operator+=(const Bitmap& b) {
        for(size_t y = 0, i = 0; y < height; ++y)
            for(size_t x = 0; x < width; ++x, ++i)
                values[i] += b.values[i];
        return *this;
    }
    void map(std::function<void (const size_t&, const size_t&, Value&)> f) {
        for(size_t y = 0, i = 0; y < height; ++y)
            for(size_t x = 0; x < width; ++x, ++i)
                f(x, y, values[i]);
    }

    void* data() { return (void*)values; }

    Value& value(size_t x, size_t y) {
        if(x>=width || x < 0 || y>=height || y<0)
            throw 1;
        return values[width*y+x];
    }
    const Value& value(size_t x, size_t y) const {
        if(x>=width || x < 0 || y>=height || y<0)
            throw 1;
        return values[width*y+x];
    }
    ~Bitmap() {
        delete[] values;
    }

    inline const size_t& getWidth() const { return width; }
    inline const size_t& getHeight() const { return height; }
};

template<typename Value>
void clamp(Bitmap<Value>& b, const Value& floor, const Value& ceil) {
    Bitmap<Value> result(b.getWidth(), b.getHeight());
    for(size_t y = 0, i = 0; y < b.getHeight(); ++y)
        for(size_t x = 0; x < b.getWidth(); ++x, ++i)
            b.values[i] = clamp(b.values[i], floor, ceil);
}

#endif // BITMAP_H
