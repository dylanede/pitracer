#ifndef SPHERE_H
#define SPHERE_H

#include "Shape.h"
#include "mymath.h"

template<typename Real>
class Sphere : public Shape<Real>
{
private:
    Real getClosest(const Ray<Real>& ray) const {
        Real r0, r1;
        unsigned char rcount;
        const Vector3<Real>& p_m_c = ray.origin;
        //double s[]= new double[3];
        Real s0, s1, s2;

        // mag2_v returns the square of the magnitude of a vector argument
        // dot_v returns the dot product of two vectors
        s2 = ray.direction.squaredLength();
        s1 = 2 * p_m_c.dot(ray.direction);
        s0 = p_m_c.squaredLength() - 1.0;

        solveQuadratic( s2, s1, s0, r0, r1, rcount);

        //get closest
        Real closest;
        switch(rcount) {
        case 0:
            closest = (Real)-1;
            break;
        case 1:
            closest = r0;
            break;
        case 2:
            closest = r0 < 0 ? r1 : (r1 < 0 ? r0: (r0 < r1 ? r0 : r1));
            break;
        }
        //if (Double.isNaN(closest))
        //        System.out.println("intersection fault! Roots were " + roots);
        return closest;
    }

public:

    typedef typename Shape<Real>::Intersection Intersection;


    virtual Intersection intersect(const Ray<Real>& ray) const {
        Real dist = getClosest(ray);
        if(dist<=0.0f) return Intersection();
        Vector3<Real> pos = ray.origin + ray.direction * dist;
        //Vector3<Real> normal = pos.normalisedCopy();
        return Intersection(pos, pos, dist, 0);
    }

    virtual Intersection intersect(const Ray<Real>& ray, Real distance) const {
        Real dist = getClosest(ray);
        if(dist<=0.0f || dist > distance) return Intersection();
        Vector3<Real> pos = ray.origin + ray.direction * dist;
        //Vector3<Real> normal = pos.normalisedCopy();
        return Intersection(pos, pos, dist, 0);
    }

    virtual bool intersects(const Ray<Real>& ray) const {
        if(ray.origin.squaredLength()<=1.0f) return true;//fast interior
        Real comp = -ray.direction.dot(ray.origin);
        if(comp>0.0f)
            return((ray.origin + (ray.direction * comp)).squaredLength()<=1.0f);
        else
            return false;
    }

    virtual bool intersects(const Ray<Real>& ray, Real distance) {
        if(distance > (Real)0 && ray.origin.squaredLength() < (Real)1) return true; //inside
        if(ray.origin.dot(ray.direction) + distance <= (Real)-1) return false;
        Real dist = getClosest(ray);
        return dist>(Real)0 & dist<distance;
    }

    virtual ~Sphere() {}
};

#endif // SPHERE_H
