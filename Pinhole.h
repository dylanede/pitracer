#ifndef PINHOLE_H
#define PINHOLE_H

#include "Camera.h"
#include "Vector2.h"
#include <cmath>

/**
  Pinhole camera pointing along positive Z, up axis along positive Y, focussing on origin
  */
template<typename Real, typename Result>
class Pinhole : public Camera<Real, Result, Vector2<Real> > {
private:
    Real width, height;
public:
    typedef Camera<Real, Result, Vector2<Real> > Parent;
    typedef typename Parent::CameraRay CameraRay;
    typedef typename Parent::Sample Sample;

    inline Pinhole(Real fov, Real aspect, const Node<Real, Result, Vector2<Real> >* parent = 0) : Camera<Real ,Result, Vector2<Real> >(parent) {
        Real f = std::sin(fov * 0.5);
        width = 2.0 * f;
        height = width / aspect;
    }

    virtual CameraRay generate(Tracer<Real, Result, Vector2<Real> >& tracer) const {
        Vector2<Real> l(tracer.random(), tracer.random());
        Vector3<Real> p((l.x-0.5)*width, (0.5-l.y)*height, 1.0);
        p.normalise();
        CameraRay result(Vector3<Real>::ZERO, p, l);
        if(Parent::parent)
            Parent::parent->modRay(result);
        return result;
    }

    virtual void filter(Sample& sample) const {
        //do nothing
    }
};

#endif // PINHOLE_H
