#ifndef LTHREAD_H
#define LTHREAD_H

#include "tinythread.h"
#include <iostream>
namespace lthread {
    /// implementation details - do not use directly
    namespace impl {
        template<typename Func>
        void use_function_once(void* _f) {
            const Func* f = (const Func*)_f;
            (*f)();
            delete f;//delete - no longer needed
        }
        template<typename Func>
        void use_function(void* _f) {
            const Func* f = (const Func*)_f;
            (*f)();
        }
    }
    /// Creates a thread based on a temporary function.
    /// Copies the function onto the heap for use outside of the local scope, removes from the heap when finished.
    template<typename Func>
    tthread::thread* new_temp_thread(const Func& f) {
        Func* _f = new Func(f);//copy to heap here
        return new tthread::thread(&impl::use_function_once<Func>, (void*)_f);
    }

    /// Creates a thread based on a guaranteed persistent function.
    /// Does not copy or delete the function.
    template<typename Func>
    tthread::thread* new_thread(const Func* f) {
        return new tthread::thread(&impl::use_function<Func>, (void*)f);
    }
}

#endif // LTHREAD_H
