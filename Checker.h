#ifndef CHECKER_H
#define CHECKER_H

#include "Material.h"
#include "mymath.h"

template<typename Real, typename Result, typename Location>
class Checker : public Material<Real, Result, Location> {
private:
    Result svalue[2];
    Real lum[2];
public:
    Checker(Result odd, Result even){
        Result value[] = { odd, even };
        for(unsigned char i = 0; i < 2; i++) {
            lum[i] = luminance(value[i]);
            svalue[i] = value[i];// / lum[i];
            //lum[i] *= 0.5;// div by lambert integral
        }
    }

    virtual Result sample(const Ray<Real>& ray, const Vector3<Real>& position, const Vector3<Real>& normal, Tracer<Real, Result, Location>& tracer) const {
        //return lerp(clamp(normal.dot(Vector3<Real>::UNIT_Y), (Real)0.0, (Real)1.0), value, Vector3<Real>::UNIT_Y);
        //Vector3<Real> fake = lerp(clamp(normal.dot(Vector3<Real>::UNIT_Y), (Real)0.0, (Real)1.0), Vector3<Real>::ZERO, value);
        Vector3<Real> fdir = pointOnCosineHS<Real>(tracer.random(), tracer.random());
        Vector3<Real> X, Y, Z;
        normal.basis(X, Y, Z);
        Vector3<Real> envdir = fdir.x * X + fdir.y * Y + fdir.z * Z;
        envdir.normalise();
        Ray<Real> envray(position + envdir * EPSILON, envdir);

        bool tile = (mod(position.x, (Real)1) > 0.5) ^ (mod(position.y, (Real)1) > 0.5) ^ (mod(position.z,(Real)1) > 0.5);
        //if(tracer.random() < lum[tile])
            return svalue[tile] * tracer.trace(envray);
        //else
        //    return Result(0.0);
        //Result envsample = tracer.trace(envray, lum * 0.5);
        //return envsample * svalue;
        //return value;
    }

    virtual ~Checker() {}
};

#endif // CHECKER_H
