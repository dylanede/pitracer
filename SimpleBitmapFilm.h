#ifndef SIMPLEBITMAPFILM_H
#define SIMPLEBITMAPFILM_H

#include "Camera.h"

template<typename Real, typename Result, typename Value>
class SimpleBitmapFilm : public BitmapFilm<Real, Result, Value> {
private:
    Bitmap<Value> bitmap;
    Bitmap<size_t> counts;
    //size_t exposure;
public:
    typedef typename Camera<Real, Result, Vector2<Real> >::Sample Sample;
    typedef BitmapFilm<Real, Result, Value> Parent;
    SimpleBitmapFilm(size_t width, size_t height) : Parent(width, height), bitmap(width, height), counts(width, height)/*, exposure(0)*/ {

    }

    virtual void expose(const Sample& s) {
        size_t x = s.loc.x * Parent::width, y = s.loc.y * Parent::height;
        if(x==Parent::width) x -= 1;
        if(y==Parent::height) y -= 1;
        size_t& count = counts.value(x, y);
        Value& value = bitmap.value(x, y);
        value = value * ((Real)count)/(count+1) + s.value/(count+1);
        count++;
        //exposure++;
    }
/*
    void merge(const SimpleBitmapFilm& film) {
        size_t size = Parent::width * Parent::height;
        //Real ppexposure = Real(exposure)/count;
        for(size_t i = 0; i < size; i++) {
            size_t& incount = film.counts.values[i];
            Value& invalue = film.bitmap.values[i];
            size_t& outcount = counts.values[i];
            Value& outvalue = bitmap.values[i];
            size_t totalcount = incount + outcount;
            outvalue = outvalue * ((Real)outcount)/totalcount + invalue * ((Real)incount)/totalcount;
            outcount = totalcount;
        }
    }
*/
    void map(std::function<void (const size_t&, const size_t&, const Value&)> f) const {
        for(size_t y = 0, i = 0; y < Parent::height; ++y)
            for(size_t x = 0; x < Parent::width; ++x, ++i)
                f(x, y, bitmap.values[i]);
    }
#if 0
    void writeResult(Bitmap<Value>& out) {
        //Bitmap<Value>* result = new Bitmap<Value>(bitmap);
        size_t count = Parent::width * Parent::height;
        //Real ppexposure = Real(exposure)/count;
        for(size_t i = 0; i < count; i++) {
            out.values[i] += bitmap.values[i]/* / ppexposure*/;
        }
        //return result;
    }
#endif
    virtual ~SimpleBitmapFilm() {}
};

#endif // SIMPLEBITMAPFILM_H
