#ifndef DIELECTIC_H
#define DIELECTIC_H

#include "Material.h"
#include "Random.h"
#include "mymath.h"
#include <iostream>
template<typename Real, typename Result, typename Location>
class Dielectric : public Material<Real, Result, Location> {
private:
    Real iorBelow, iorAbove;
public:
    Dielectric(Real iorBelow, Real iorAbove) : iorBelow(iorBelow), iorAbove(iorAbove) {
    }

    virtual Result sample(const Ray<Real>& ray, const Vector3<Real>& position, const Vector3<Real>& _normal, Tracer<Real, Result, Location>& tracer) const {
        //std::cout << "In dielectric" << std::endl;
        Vector3<Real> normal = _normal;
        Real mRdotN = -normal.dot(ray.direction);
        Real pior, nior;
        Vector3<Real> factor;
        if(mRdotN > Real(0)) {//coming from outside
            pior = iorAbove;
            nior = iorBelow;
            //mRdotN = -mRdotN;

        } else {//coming from inside
            pior = iorBelow;
            nior = iorAbove;
            normal = -normal;
        }

        Real refcomp = fresnelReflect(pior, nior, -ray.direction.dot(normal));
        if(tracer.random() < refcomp) {
            Vector3<Real> refdir = ray.direction.reflect(normal);
            Ray<Real> refray(position + refdir * EPSILON, refdir);
            Result refsample = tracer.trace(refray);
            //std::cout << "Exiting dielectric" << std::endl;
            return refsample;
        } else {
            Real n = pior/nior;
            Real cosI = mRdotN;
            Real cosT2 = Real(1) - n * n * (Real(1) - cosI * cosI);
            //if(cosT2 > Real(0)) {
                Vector3<Real> refdir = ray.direction * n + normal * (n * cosI - sqrt(cosT2));
                Ray<Real> refray(position + refdir * EPSILON, refdir);
                Result refsample = tracer.trace(refray);
                //std::cout << "Exiting dielectric" << std::endl;
                return refsample;
            //}
        }
    }

    virtual ~Dielectric() {}
};

#endif // DIELECTIC_H
