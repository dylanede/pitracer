#ifndef RANDOM_H
#define RANDOM_H
#if 0
#include "stdlib.h"

template<typename Real>
Real randr() {
    return (Real)rand() / (Real)RAND_MAX;
}
#else

#include "CmRandomNumberGenerator.h"

template<typename Real>
Real randr(CmRandomNumberGenerator* rng);

template<>
float randr(CmRandomNumberGenerator* rng) {
    return rng->getFloat();
}
template<>
double randr(CmRandomNumberGenerator* rng) {
    return rng->getDouble();
}

#endif
#endif // RANDOM_H
