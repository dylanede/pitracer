TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    Vector3.cpp \
    Quaternion.cpp \
    ShapeInstance.cpp \
    SimpleTracer.cpp \
    Transform.cpp \
    lodepng.cpp \
    CmRandomNumberGenerator.cpp \
    tinythread.cpp

OTHER_FILES += \
    Makefile

HEADERS += \
    Vector3.h \
    Matrix.h \
    Quaternion.h \
    LinearSpace.h \
    Shape.h \
    Ray.h \
    ShapeInstance.h \
    Material.h \
    Tracer.h \
    SimpleTracer.h \
    Node.h \
    Transform.h \
    Camera.h \
    Group.h \
    Bitmap.h \
    sRGB.h \
    ConstantEmission.h \
    Pinhole.h \
	Vector2.h \
    Random.h \
    lodepng.h \
    CmRandomNumberGenerator.h \
    Sphere.h \
    mymath.h \
    TestMaterial.h \
    Plane.h \
    TestReflect.h \
    Dielectric.h \
    Metropolis.h \
    Checker.h \
    tinythread.h \
    fast_mutex.h \
    SimpleBitmapFilm.h \
    KahanBitmapFilm.h \
    lthread.h \
    MetropolisTracer.h

target = PiTracer
target.path=/root/PiTracer/
INSTALLS = target
