#ifndef METROPOLISTRACER_H
#define METROPOLISTRACER_H

#include <vector>
#include <stack>
#include <utility>
#include "mymath.h"
#include "Tracer.h"
#include "Camera.h"
#include "Node.h"
#include "Random.h"
/**
  Metropolis Light Transport à la Kelemen et al. 2002
  Includes russian roulette, importance sampling.
  Is not bidirectional due to requirement imposed on emissive objects.
  */
template<typename Real, typename Result, typename Location>
class MetropolisTracer : public Tracer<Real, Result, Location>
{
private:
    Node<Real, Result, Location>* root;
    Material<Real, Result, Location>* nohit;
    size_t depth;
    size_t minPathLength;
    Real plarge;
    CmRandomNumberGenerator* rng;

    typedef struct Coord{
        Real value;
        size_t modify;
        inline Coord(const Real& value, const size_t& modify) : value(value), modify(modify) {}
    } Coord;
    std::vector<Coord> u;

    size_t time, large_step_time;
    bool large_step;

    typedef struct Change {
        size_t i;
        Coord coord;
        inline Change(const size_t& i, const Coord& coord) : i(i), coord(coord) {}
    } Change;
    std::stack<Change> mutation;
    std::vector<Change> premutation;

    typedef typename Camera<Real, Result, Location>::Sample Sample;
    typedef typename Camera<Real, Result, Location>::WeightedSample WeightedSample;

    WeightedSample oldsample, newsample, contribsample;
    Real oldI, totalI;
    size_t large_step_count;

    inline void Push(size_t i) {
        //mutation.push(Change(i, u[i]));
        premutation.push_back(Change(i, u[i]));
    }

    inline void Pop() {
        //Change c = mutation.top();
        //u[c.i] = c.coord;
        //mutation.pop();
        const Change& c = premutation.back();
        u[c.i] = c.coord;
        premutation.pop_back();
    }

    inline bool isStackEmpty() {
        return premutation.empty();
    }

    inline void ClearStack() {
        while(!premutation.empty())
            premutation.pop_back();
    }

    inline Real pSmallXtoY(Real x, Real y) {
        Real s1 = Real(1./1024), s2 = Real(1./64);
        Real dv = std::abs(x-y);
        Real rnd = std::log(s2/dv)/std::log(s1/dv);
        //return
    }

    Real Mutate(Real& value) {
        Real s1 = Real(1./1024), s2 = Real(1./64);//s2 = Real(1./64);
        Real r = randr<Real>(rng);
        bool add;
        if(r < Real(0.5)) {
            add = false;
            r *= Real(2);
        } else {
            add = true;
            r = (r-Real(0.5)) * Real(2);
        }

        Real dv = s2 * std::pow(s2/s1, -r);
        //Real dv = s2 * std::exp(-std::log(s2/s1)*randr<Real>(rng));
        if(add) {
            value += dv; if(value > Real(1)) value -= Real(1);
        } else {
            value -= dv; if(value < Real(0)) value += Real(1);
        }
    }

    /// PrimarySample generates the value to be used for the random variable of index i in the path
    Real PrimarySample(const size_t& i) {
        // lazy initialisation
        if(i > u.size()) throw 1;
        if(i == u.size())
            u.push_back(Coord(0,0));
        if(u[i].modify < time) {
            if(large_step) {
                Push(i);
                u[i].modify = time;
                u[i].value = randr<Real>(rng);
            } else {
                if(u[i].modify < large_step_time) {
                    u[i].modify = large_step_time;
                    u[i].value = randr<Real>(rng);
                }
                while(u[i].modify < time - 1) {
                    Mutate(u[i].value);
                    u[i].modify++;
                }
                Push(i);
                Mutate(u[i].value);
                u[i].modify++;
            }
        }
        return u[i].value;
    }
    /*
    Sample Next(const Real& I, const Sample& contrib) {
        newsample.contrib = contrib;
        newsample.w = I == Real(0) ? Real(0) : Real(1)/I;
        Real a = std::min(Real(1), I/oldI);
        if(randr<Real>(rng) < a) {
            oldsample = newsample;
            contribsample = newsample;
            oldI = I;
            time++;
            ClearStack();
        } else {
            contribsample = oldsample;
            while(!isStackEmpty()) Pop();
        }
        large_step = randr<Real>(rng) < plarge;
        return Sample(contribsample.contrib.loc, contribsample.contrib.dir, contribsample.contrib.value * contribsample.w);
    }
    */
    inline Real min(const Real& a, const Real& b) {
        return a < b ? a : b;
    }

    /// I is the luminance of the just-generated sample.
    /// contrib is the sample itself, including location
    /// Next picks the sample to return for this iteration, and weights it
    WeightedSample Next(Real I, const Sample& contrib) {
        /*Real Txtoy = Real(1);//, Tytox = Real(1);
        if(!large_step) {
            for(auto itr = premutation.begin(); itr != premutation.end(); itr++) {
                const Real& y = u[itr->i].value;
                const Real& x = itr->coord.value;
                Txtoy *= pSmallXtoY(x, y);
                //Tytox *= pSmallXtoY(y, x);
            }
            I /= Txtoy;
        }*/
        if(large_step) {
            totalI += I;
            large_step_count++;
        }
        //Real b = totalI / large_step_count;
        //if(b<=Real(0)) b = Real(1);
        Real b = totalI > Real(0) ? totalI / large_step_count : Real(1);

        Real a = oldI == Real(0) ? Real(1) : min(Real(1), I/oldI);
        newsample.contrib = contrib;
        //newsample.w = I==Real(0) ? Real(0) : b * a / I;
        //oldsample.w += oldI==Real(0) ? Real(0) : b * (1-a) / oldI;
        newsample.w = (a+large_step)/(I/b + plarge);///M;
        oldsample.w += (1-a)/(oldI/b + plarge);///M;
        //oldsample.w = a;
        //newsample.w = Real(1) - a;
        if(randr<Real>(rng) < a) {
            oldI = I;
            contribsample = oldsample;
            oldsample = newsample;
            if(large_step) large_step_time = time;
            time++;
            ClearStack();
        } else {
            contribsample = newsample;
            while(!isStackEmpty()) Pop();
        }
        large_step = randr<Real>(rng) < plarge;
        return contribsample;
        //return Sample(contribsample.contrib.loc, contribsample.contrib.dir, contribsample.contrib.value * contribsample.w);
    }

    MetropolisTracer(const MetropolisTracer&);
    MetropolisTracer& operator=(const MetropolisTracer&);
public:

    MetropolisTracer(Material<Real, Result, Location>* nohit, Node<Real, Result, Location>* root = 0, size_t minPathLength = 5, Real plarge = Real(0.1)) :
        nohit(nohit), minPathLength(minPathLength), plarge(plarge), root(root), depth(0), rng(new CmRandomNumberGenerator()),
        time(1), large_step_time(0), large_step(true), oldsample(), newsample(), oldI(0), totalI(0), large_step_count(0)
    {

    }
    typedef typename Node<Real, Result, Location>::Intersection Intersection;
    Result trace(const Ray<Real>& ray) {
        depth++;
        Result r;
        //if(weight < randr<Real>())
        //    return Result(0.0);
        bool rr = false;
        if(depth > minPathLength)
            rr = true;
        if(rr && random() < Real(0.5))
            r = Result(0.0);
        else
            if(root) {
                Intersection i = root->intersect(ray);
                if(i.hit)
                    r = i.material->sample(ray, i.position, i.normal, *this);
                else
                    r = nohit->sample(ray, Vector3<Real>::ZERO, ray.direction, *this);
            } else
                r = nohit->sample(ray, Vector3<Real>::ZERO, ray.direction, *this);
        if(rr)
            r *= Real(2.0);
        depth--;
        return r;
    }

    size_t currentRV;
    typedef Camera<Real, Result, Location> _Camera;
    WeightedSample sample(const _Camera& camera, const typename _Camera::Film& film) {
        currentRV = 0;
        typename _Camera::CameraRay ray = camera.generate(*this);
        Result result = trace(ray);
        Sample _s(ray.loc, ray.direction, result);
        WeightedSample s = Next(film.importance(_s), _s);
        camera.filter(s);
        return s;
    }

    Real random() {
        //Real result = randr<Real>(rng);
        //if(result < Real(0) || result > Real(1))
        //    throw 5;
        //return result;
        return PrimarySample(currentRV++);
    }
    void seed(unsigned long seed) {
        rng->changeSeed(seed);
    }

    void setRoot(Node<Real, Result, Location>* root) { this->root = root; }

    virtual ~MetropolisTracer() {
        delete rng;
    }
};

#endif // METROPOLISTRACER_H
