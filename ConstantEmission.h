#ifndef CONSTANTEMISSION_H
#define CONSTANTEMISSION_H

#include "Material.h"

template<typename Real, typename Result, typename Location>
class ConstantEmission : public Material<Real, Result, Location> {
private:
    Result value;
public:
    ConstantEmission(Result value) : value(value) {}

    virtual Result sample(const Ray<Real>& ray, const Vector3<Real>& position, const Vector3<Real>& normal, Tracer<Real, Result, Location>& tracer) const {
        //return lerp(clamp(normal.dot(Vector3<Real>::UNIT_Y), (Real)0.0, (Real)1.0), value, Vector3<Real>::UNIT_Y);
        return value;
    }

    virtual ~ConstantEmission() {}
};

#endif // CONSTANTEMISSION_H
