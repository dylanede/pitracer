#ifndef RAY_H
#define RAY_H

#include "Vector3.h"

template<typename Real>
struct Ray {
    Vector3<Real> origin, direction;

    inline Ray(Vector3<Real> origin, Vector3<Real> direction) : origin(origin), direction(direction) {}
};

#endif // RAY_H
