#ifndef PATHTRACER_H
#define PATHTRACER_H

#include "Tracer.h"
#include "Node.h"
#include "Random.h"
/**
  Straight forward path tracer that does not mutate.
  Just performs Russian Roulette sampling.
  */
template<typename Real, typename Result, typename Location>
class SimpleTracer : public Tracer<Real, Result, Location>
{
private:
    Node<Real, Result, Location>* root;
    Material<Real, Result, Location>* nohit;
    size_t depth;
    size_t minPathLength;
    CmRandomNumberGenerator* rng;

    SimpleTracer(const SimpleTracer&);
    SimpleTracer& operator=(const SimpleTracer&);
public:
    //SimpleTracer(const SimpleTracer& t) : root(t.root), nohit(t.nohit), depth(0), minPathLength(t.minPathLength), rng(new CmRandomNumberGenerator()) {

    //}

    SimpleTracer(Material<Real, Result, Location>* nohit, Node<Real, Result, Location>* root = 0, size_t minPathLength = 5) : nohit(nohit), minPathLength(minPathLength), root(root), depth(0), rng(new CmRandomNumberGenerator()) {

    }
    typedef typename Node<Real, Result, Location>::Intersection Intersection;
    Result trace(const Ray<Real>& ray) {
        depth++;
        Result r;
        //if(weight < randr<Real>())
        //    return Result(0.0);
        bool rr = false;
        if(depth > minPathLength)
            rr = true;
        if(rr && random() < Real(0.5))
            r = Result(0.0);
        else
            if(root) {
                Intersection i = root->intersect(ray);
                if(i.hit)
                    r = i.material->sample(ray, i.position, i.normal, *this);
                else
                    r = nohit->sample(ray, Vector3<Real>::ZERO, ray.direction, *this);
            } else
                r = nohit->sample(ray, Vector3<Real>::ZERO, ray.direction, *this);
        if(rr)
            r *= Real(2.0);
        depth--;
        return r;
    }

    typedef Camera<Real, Result, Location> _Camera;
    typedef typename _Camera::Sample Sample;
    Sample sample(const _Camera& camera) {
        typename _Camera::CameraRay ray = camera.generate(*this);
        Result result = trace(ray);
        Sample s(ray.loc, ray.direction, result);
        camera.filter(s);
        return s;
    }

    Real random() {
        Real result = randr<Real>(rng);
        if(result < Real(0) || result > Real(1))
            throw 5;
        return result;
    }
    void seed(unsigned long seed) {
        rng->changeSeed(seed);
    }

    void setRoot(Node<Real, Result, Location>* root) { this->root = root; }

    virtual ~SimpleTracer() {
        delete rng;
    }
};
#endif // PATHTRACER_H
