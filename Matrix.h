#ifndef MATRIX_H
#define MATRIX_H

template<typename Real, unsigned int Size>
class Matrix {
    Real v[Size][Size];
};

#endif // MATRIX_H
