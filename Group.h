#ifndef GROUP_H
#define GROUP_H

#include "Node.h"
#include <vector>

template<typename Real, typename Result, typename Location>
class Group : public Node<Real, Result, Location> {
private:
    std::vector<const Node<Real, Result, Location>*> nodes;
    typedef typename std::vector<const Node<Real, Result, Location>*>::const_iterator Iterator;
public:
    typedef typename Node<Real, Result, Location>::Intersection Intersection;

    inline Group(Node<Real, Result, Location>* parent = 0) : Node<Real, Result, Location>(parent) {}

    inline void addNode(const Node<Real, Result, Location>* node) { nodes.push_back(node); }

    virtual Intersection intersect(const Ray<Real>& ray) const {

        Intersection closest;
        for(Iterator i = nodes.begin(); i != nodes.end(); ++i) {
            Intersection tentative = (*i)->intersect(ray);
            if(!closest.hit || tentative.d < closest.d)
                closest = tentative;
        }

        return closest;
    }
    virtual Intersection intersect(const Ray<Real>& ray, Real distance) const {
        Intersection closest;
        for(Iterator i = nodes.begin(); i != nodes.end(); ++i) {
            Intersection tentative = (*i)->intersect(ray, distance);
            if(!closest.hit || tentative.d < closest.d)
                closest = tentative;
        }
        return closest;
    }
    virtual bool intersects(const Ray<Real>& ray) const {
        for(Iterator i = nodes.begin(); i != nodes.end(); ++i)
            if((*i)->intersects(ray))
                return true;
        return false;
    }
    virtual bool intersects(const Ray<Real>& ray, Real distance) const {
        for(Iterator i = nodes.begin(); i != nodes.end(); ++i)
            if((*i)->intersects(ray, distance))
                return true;
        return false;
    }
};

#endif // GROUP_H
