#ifndef VECTOR3_H
#define VECTOR3_H
#include <cmath>

template<typename Real>
class Vector3 {
public:

    union {
        struct {
            Real x, y, z;
        };
        Real _v[3];
    };

    inline Vector3() : x(0), y(0), z(0) {}
    inline Vector3(Real v) : x(v), y(v), z(v) {}
    inline Vector3(Real x, Real y, Real z) : x(x), y(y), z(z) {}
    inline Vector3(Real* v) : x(v[0]), y(v[1]), z(v[2]) {}

    static const Vector3 ZERO, UNIT, UNIT_X, UNIT_Y, UNIT_Z;

    inline Vector3 operator-(const Vector3& v) const { return Vector3(x-v.x, y-v.y, z-v.z); }
    inline Vector3 operator-(const Real& v) const { return Vector3(x-v, y-v, z-v); }
    inline Vector3 operator-() { return Vector3(-x, -y, -z); }
    inline Vector3 operator+(const Vector3& v) const { return Vector3(x+v.x, y+v.y, z+v.z); }
    inline Vector3 operator+(const Real& v) const { return Vector3(x+v, y+v, z+v); }
    inline Vector3 operator*(const Vector3& v) const { return Vector3(x*v.x, y*v.y, z*v.z); }
    inline Vector3 operator*(const Real& v) const { return Vector3(x*v, y*v, z*v); }
    inline Vector3 operator/(const Vector3& v) const { return Vector3(x/v.x, y/v.y, z/v.z); }
    inline Vector3 operator/(const Real& v) const { return Vector3(x/v, y/v, z/v); }

    inline Vector3& operator-=(const Vector3& v) { x-=v.x, y-=v.y, z-=v.z; return *this; }
    inline Vector3& operator-=(const Real& v) { x-=v, y-=v, z-=v; return *this; }
    inline Vector3& operator+=(const Vector3& v) { x+=v.x, y+=v.y, z+=v.z; return *this; }
    inline Vector3& operator+=(const Real& v) { x+=v, y+=v, z+=v; return *this; }
    inline Vector3& operator*=(const Vector3& v) { x*=v.x, y*=v.y, z*=v.z; return *this; }
    inline Vector3& operator*=(const Real& v) { x*=v, y*=v, z*=v; return *this; }
    inline Vector3& operator/=(const Vector3& v) { x/=v.x, y/=v.y, z/=v.z; return *this; }
    inline Vector3& operator/=(const Real& v) { x/=v, y/=v, z/=v; return *this; }

    inline Real dot(const Vector3& v) const { return x*v.x + y*v.y + z*v.z; }
    inline Vector3 cross(const Vector3& v) const { return Vector3(y*v.z-v.y*z, z*v.x-v.z*x, x*v.y-v.x*y); }

    inline Real squaredLength() const { return dot(*this); }
    inline Real length() const { return std::sqrt(squaredLength()); }

    inline void normalise() {
        Real l = length();
        x /= l; y /= l; z /= l;
    }

    inline Vector3 normalisedCopy() const {
        Real l = length();
        return Vector3(x/l, y/l, z/l);
    }

    inline Vector3 reflect(const Vector3& N) const { return *this - N * ((dot(N) * 2)); }

    inline Real distance(const Vector3& v) const { return (*this - v).length(); }

    void basis(Vector3& X, Vector3& Y, Vector3& Z) const {
        /*Vector3 axis;
        if(x < y && x < z)
            axis = Vector3::UNIT_X;
        else if (y < z)
            axis = Vector3::UNIT_Y;
        else
            axis = Vector3::UNIT_Z;
        X = cross(axis);
        X.normalise();
        Y = cross(X);
        Z = *this;
        */
        Z = *this;
        if(std::abs(x) > std::abs(y)) {
            Real invLen = Real(1) / std::sqrt(x*x + z*z);
            Y = Vector3(z * invLen, 0, -x * invLen);
        } else {
            Real invLen = Real(1) / std::sqrt(y*y + z*z);
            Y = Vector3(0, z * invLen, -y * invLen);
        }
        X = Y.cross(Z);
    }
};



template<typename Real>
inline Real clamp(const Real& v, const Real& floor, const Real& ceil) {
    if(v <= ceil)
        if(v >= floor)
            return v;
        else
            return floor;
    else
        return ceil;
}

template <typename Real> Real luminance(const Vector3<Real>& linear) {
    return linear.x * 0.2126 + linear.y * 0.7152 + linear.z * 0.0722;
}

template<typename Real>
inline Real lerp(const Real& v, const Real& a, const Real& b) { return a + (b-a) * v; }


template<typename Real>
inline Vector3<Real> clamp(const Vector3<Real>& v, const Vector3<Real>& floor, const Vector3<Real>& ceil) {
    return Vector3<Real>(clamp(v.x,floor.x,ceil.x),
                         clamp(v.y,floor.y,ceil.y),
                         clamp(v.z,floor.z,ceil.z));
}

template<typename Real>
inline Vector3<Real> lerp(const Real& v, const Vector3<Real>& a, const Vector3<Real>& b) {
    return Vector3<Real>(lerp(v,a.x,b.x), lerp(v,a.y,b.y), lerp(v,a.z,b.z));
}

template<typename Real>
inline Vector3<Real> operator+(const Real& r, const Vector3<Real>& v) { return Vector3<Real>(r+v.x, r+v.y, r+v.z); }
template<typename Real>
inline Vector3<Real> operator-(const Real& r, const Vector3<Real>& v) { return Vector3<Real>(r-v.x, r-v.y, r-v.z); }
template<typename Real>
inline Vector3<Real> operator*(const Real& r, const Vector3<Real>& v) { return Vector3<Real>(r*v.x, r*v.y, r*v.z); }
template<typename Real>
inline Vector3<Real> operator/(const Real& r, const Vector3<Real>& v) { return Vector3<Real>(r/v.x, r/v.y, r/v.z); }

typedef Vector3<float> Vector3f;
typedef Vector3<double> Vector3d;

#ifdef REAL
typedef Vector3<REAL> Vector3r;
#else
typedef Vector3<float> Vector3r;
#endif

template<>
const Vector3<float> Vector3<float>::ZERO = Vector3<float>(0.0f);
template<>
const Vector3<float> Vector3<float>::UNIT = Vector3<float>(1.0f, 1.0f, 1.0f);
template<>
const Vector3<float> Vector3<float>::UNIT_X = Vector3<float>(1.0f, 0.0f, 0.0f);
template<>
const Vector3<float> Vector3<float>::UNIT_Y = Vector3<float>(0.0f, 1.0f, 0.0f);
template<>
const Vector3<float> Vector3<float>::UNIT_Z = Vector3<float>(0.0f, 0.0f, 1.0f);

template<>
const Vector3<double> Vector3<double>::ZERO = Vector3<double>(0.0);
template<>
const Vector3<double> Vector3<double>::UNIT = Vector3<double>(1.0, 1.0, 1.0);
template<>
const Vector3<double> Vector3<double>::UNIT_X = Vector3<double>(1.0, 0.0, 0.0);
template<>
const Vector3<double> Vector3<double>::UNIT_Y = Vector3<double>(0.0, 1.0, 0.0);
template<>
const Vector3<double> Vector3<double>::UNIT_Z = Vector3<double>(0.0, 0.0, 1.0);

#endif // VECTOR3_H
