#ifndef KAHANBITMAPFILM_H
#define KAHANBITMAPFILM_H

#include "Camera.h"

template<typename Real, typename Result, typename Value>
class KahanBitmapFilm : public BitmapFilm<Real, Result, Value> {
private:
    Bitmap<Value> bitmap;
    Bitmap<Real> counts;
    Bitmap<Value> compensations;
    //size_t exposure;
public:
    typedef typename Camera<Real, Result, Vector2<Real> >::WeightedSample WeightedSample;
    typedef typename Camera<Real, Result, Vector2<Real> >::Sample Sample;
    typedef BitmapFilm<Real, Result, Value> Parent;
    KahanBitmapFilm(size_t width, size_t height) : Parent(width, height), bitmap(width, height), counts(width, height), compensations(width, height)/*, exposure(0)*/ {

    }

    virtual void expose(const WeightedSample& s) {
        size_t x = s.contrib.loc.x * Parent::width, y = s.contrib.loc.y * Parent::height;
        if(x==Parent::width) x -= 1;
        if(y==Parent::height) y -= 1;
        Real& count = counts.value(x, y);
        Value& sum = bitmap.value(x, y);
        Value& c = compensations.value(x, y);
        Value d = Value(s.contrib.value * s.w) - c;
        Value t = sum + d; // should disable arithmetic optimisations for this to work
        c = (t - sum) - d;
        sum = t;
        //value = value * ((Real)count)/(count+1) + s.value/(count+1);
        count+=s.w;
        //exposure++;
    }
    virtual Real importance(const Sample& s) const {
        return luminance(s.value);
    }

    void merge(const KahanBitmapFilm& film) {
        size_t size = Parent::width * Parent::height;
        //Real ppexposure = Real(exposure)/count;
        for(size_t i = 0; i < size; i++) {
            Real& incount = film.counts.values[i];
            Value& invalue = film.bitmap.values[i];
            Real& outcount = counts.values[i];
            Value& outvalue = bitmap.values[i];
            Real totalcount = incount + outcount;
            outvalue = outvalue * ((Real)outcount)/totalcount + invalue * incount/totalcount;
            outcount = totalcount;
        }
    }

    void map(std::function<void (const size_t&, const size_t&, const Value&)> f) const {
        for(size_t y = 0, i = 0; y < Parent::height; ++y)
            for(size_t x = 0; x < Parent::width; ++x, ++i)
                f(x, y, counts.values[i]==Real(0) ? Value(Real(0)) : bitmap.values[i] / counts.values[i]);
    }

    virtual ~KahanBitmapFilm() {}
};

#endif // KAHANBITMAPFILM_H
