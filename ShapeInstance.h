#ifndef SHAPEINSTANCE_H
#define SHAPEINSTANCE_H

#include <exception>
#include "Node.h"
#include "Shape.h"

template<typename Real, typename Result, typename Location>
class Material;

const class MaterialIdOutOfBoundsException : public std::exception
{
    virtual const char* what() const throw()
    {
        return "Material ID out of bounds";
    }
} materialIdOutOfBoundsException;

template<typename Real, typename Result, typename Location>
class ShapeInstance : public Node<Real, Result, Location> {
private:
    Shape<Real>* shape;
    Material<Real, Result, Location>* mats;
    int matCount;
    inline bool inBounds(size_t i) const { return i >= 0 && i < matCount; }
public:

    inline ShapeInstance(Shape<Real>* shape, Material<Real,Result,Location>* mats, int matCount) : shape(shape), mats(mats), matCount(matCount) {}

    inline virtual typename Node<Real, Result,Location>::Intersection intersect(const Ray<Real>& ray) const {
        struct Shape<Real>::Intersection i = shape->intersect(ray);
        if(!i.hit) return typename Node<Real, Result,Location>::Intersection();
        if(inBounds(i.material_id))
            return typename Node<Real, Result,Location>::Intersection(i.position, i.normal, i.d, mats + i.material_id);
        else
            throw materialIdOutOfBoundsException;
    }

    inline virtual typename Node<Real, Result, Location>::Intersection intersect(const Ray<Real>& ray, Real distance) const {
        struct Shape<Real>::Intersection i = shape->intersect(ray,distance);
        if(!i.hit) return typename Node<Real, Result,Location>::Intersection();
        if(inBounds(i.material_id))
            return typename Node<Real, Result,Location>::Intersection(i.position, i.normal, i.d, mats + i.material_id);
        else
            throw materialIdOutOfBoundsException;
    }
    inline virtual bool intersects(const Ray<Real>& ray) const {
        return shape->intersects(ray);
    }
    inline virtual bool intersects(const Ray<Real>& ray, Real distance) const {
        return shape->intersects(ray, distance);
    }
};

#endif // SHAPEINSTANCE_H
