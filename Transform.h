#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "Vector3.h"
#include "Quaternion.h"
#include "Node.h"

template<typename Real, typename Result, typename Location>
class Transform : public Node<Real, Result, Location>
{
private:
    Vector3<Real> scale, position;
    Quaternion<Real> orientation, orientation_i;
    const Node<Real, Result, Location>* child;
    Vector3<Real> untransformPosition(const Vector3<Real>& p) const {
        return (p * scale) * orientation + position;
    }
    Vector3<Real> untransformDirection(const Vector3<Real>& d) const {
        return (d * scale) * orientation;
    }
    Vector3<Real> transformPosition(const Vector3<Real>& p) const {
        return ((p - position) * orientation_i) / scale;
    }
    Vector3<Real> transformDirection(const Vector3<Real>& d) const {
        return (d * orientation_i) / scale;
    }
    Vector3<Real> transformNormal(const Vector3<Real>& n) const {
        return (n * orientation_i) * scale;
    }
    Vector3<Real> untransformNormal(const Vector3<Real>& n) const {
        return (n / scale) * orientation;
    }
public:
    typedef typename Node<Real, Result, Location>::Intersection Intersection;

    inline Transform(const Vector3<Real>& scale, const Quaternion<Real> orientation, const Vector3<Real>& position, Node<Real, Result, Location>* parent = 0)
        : scale(scale), orientation(orientation), orientation_i(orientation.inverse()), position(position), Node<Real, Result, Location>(parent) {}
    inline const Node<Real, Result, Location>* setChild(const Node<Real, Result, Location>* node) { return child = node; }
    inline Node<Real, Result, Location>* getChild() { return child; }

    virtual Intersection intersect(const Ray<Real>& ray) const {
        Vector3<Real> dir = transformDirection(ray.direction);
        Real lscale = 1.0 / dir.length();
        Ray<Real> _ray = Ray<Real>(transformPosition(ray.origin), dir * lscale);
        Intersection result = child->intersect(_ray);
        if(result.hit)
            return Intersection(untransformPosition(result.position), untransformNormal(result.normal).normalisedCopy(), lscale * result.d, result.material);
        else
            return Intersection();
    }
    virtual typename Node<Real, Result, Location>::Intersection intersect(const Ray<Real>& ray, Real distance) const {
        Vector3<Real> dir = transformDirection(ray.direction);
        Real lscale = 1.0 / dir.length();
        Ray<Real> _ray = Ray<Real>(transformPosition(ray.origin), dir * lscale);
        Intersection result = child->intersect(_ray, (dir * distance).length());
        if(result.hit)
            return Intersection(untransformPosition(result.position), untransformNormal(result.normal).normalisedCopy(), lscale * result.d, result.material);
        else
            return Intersection();
    }
    virtual bool intersects(const Ray<Real>& ray) const {
        Ray<Real> _ray = Ray<Real>(transformPosition(ray.origin),transformDirection(ray.direction).normalisedCopy());
        return child->intersects(_ray);
    }
    virtual bool intersects(const Ray<Real>& ray, Real distance) const {
        Vector3<Real> dir = transformDirection(ray.direction);
        Ray<Real> _ray = Ray<Real>(transformPosition(ray.origin),dir.normalisedCopy());
        return child->intersects(_ray, (dir * distance).length());
    }
    virtual void modRay(Ray<Real>& ray) const {
        ray.origin = untransformPosition(ray.origin);
        ray.direction = untransformDirection(ray.direction).normalisedCopy();
        if(Node<Real, Result, Location>::parent)
            Node<Real, Result, Location>::parent->modRay(ray);
    }
    virtual void modRay(Ray<Real>& ray, Real& distance) const {
        ray.direction = untransformDirection(ray.direction);
        distance = (ray.direction * distance).length();
        ray.direction = ray.direction.normalisedCopy();
        ray.origin = untransformPosition(ray.origin);
        if(Node<Real, Result, Location>::parent)
            Node<Real, Result, Location>::parent->modRay(ray);
    }
};

#endif // TRANSFORM_H
