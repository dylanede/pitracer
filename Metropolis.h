#ifndef METROPOLIS_H
#define METROPOLIS_H

#include "Tracer.h"
#include "Node.h"
#include "Random.h"
/**
  Kelemen-style Metropolis sampler, does not use bidirectional tracing.
  */
template<typename Real, typename Result>
class SimpleTracer : public Tracer<Real, Result>
{
private:
    Node<Real, Result>* root;
    Material<Real, Result>* nohit;
    size_t depth;
    size_t minPathLength;
public:
    SimpleTracer(Material<Real, Result>* nohit, size_t minPathLength = 5, Node<Real, Result>* root = 0) : nohit(nohit), minPathLength(minPathLength), root(root), depth(0) {

    }
    typedef typename Node<Real, Result>::Intersection Intersection;
    Result trace(const Ray<Real>& ray) {
        if(!depth) {//new sample

        }

        depth++;
        Result r;
        //if(weight < randr<Real>())
        //    return Result(0.0);
        bool rr = false;
        if(depth > minPathLength)
            rr = true;
        if(rr && random() < Real(0.5))
            r = Result(0.0);
        else
            if(root) {
                Intersection i = root->intersect(ray);
                if(i.hit)
                    r = i.material->sample(ray, i.position, i.normal, *this);
                else
                    r = nohit->sample(ray, Vector3<Real>::ZERO, ray.direction, *this);
            } else
                r = nohit->sample(ray, Vector3<Real>::ZERO, ray.direction, *this);
        if(rr)
            r *= Real(2.0);
        depth--;
        return r;
    }
    Real random() {
        return randr<Real>();
    }

    void setRoot(Node<Real, Result>* root) { this->root = root; }
};

#endif // METROPOLIS_H
