#ifndef PLANE_H
#define PLANE_H

template<typename Real>
class Plane : public Shape<Real>
{
public:
    typedef typename Shape<Real>::Intersection Intersection;
    virtual Intersection intersect(const Ray<Real>& ray) const {
        if(ray.direction.y * ray.origin.y<0)
        {
            double t = -ray.origin.y/ray.direction.y;
            Vector3<Real> pos = ray.origin + ray.direction * t;
            Vector3<Real> normal = Vector3<Real>(0,signum(ray.origin.y),0);
            return Intersection(pos, normal, t, 0);
        } else return Intersection();
    }

    virtual Intersection intersect(const Ray<Real>& ray, Real distance) const {
        if(ray.direction.y * ray.origin.y<0)
        {
            double t = -ray.origin.y/ray.direction.y;
            if(t < distance) return Intersection();
            Vector3<Real> pos = ray.origin + ray.direction * t;
            Vector3<Real> normal = Vector3<Real>(0,signum(ray.origin.y),0);
            return Intersection(pos, normal, t, 0);
        } else return Intersection();
    }

    virtual bool intersects(const Ray<Real>& ray) const {
        if(ray.direction.y * ray.origin.y < 0)
            return true;
        else
            return false;
    }

    virtual bool intersects(const Ray<Real>& ray, Real distance) {
        if(ray.direction.y * ray.origin.y < 0)
        {
            if(-ray.origin.y / ray.direction.y < distance)
                return true;
            else
                return false;
        } else return false;
    }

    virtual ~Plane() {}
};

#endif // PLANE_H
