#ifndef MYMATH_H
#define MYMATH_H

#include <limits>
#include <cmath>
#include "Vector3.h"

#define PI (3.14159265)

#define EPSILON 0.000001

template<typename Real>
inline Real mod(const Real& n, const Real& d) {
    if(d==0) return n;
    return n - d * std::floor(n / d);
}

template<typename Real>
Vector3<Real> pointOnSphere(const Real& u, const Real& v) {
    Real _u = Real(2) * u - Real(1);
    Real theta = Real(2 * PI) * v;
    Real s1mu2 = Real(sqrt(Real(1) - _u * _u));
    return Vector3<Real>(
                s1mu2 * std::cos(theta),
                s1mu2 * std::sin(theta),
                _u);
}

template<typename Real>
Vector3<Real> pointOnCircle(const Real& u, const Real& v) {
    /*Real r = Real(sqrt(u));
    Real theta = v * (PI * 2);
    return Vector3<Real>(
                r * std::cos(theta),
                r * std::sin(theta),
                Real(0));*/
    Real r1 = 2 * u - 1;
    Real r2 = 2 * v - 1;

    Vector2<Real> coords;
    if(r1 == Real(0) && r2 == Real(0)) {
        coords = Vector2<Real>(0, 0);
    } else if(r1 > -r2) {
        if( r1 > r2)
            coords = Vector2<Real>(r1, (PI/4) * r2/r1);
        else
            coords = Vector2<Real>(r2, (PI/4) * (2 - r1/r2));
    } else {
        if(r1 < r2)
            coords = Vector2<Real>(-r1, (PI/4) * (4 + r2/r1));
        else
            coords = Vector2<Real>(-r2, (PI/4) * (6 - r1/r2));
    }

    return Vector3<Real>(coords.x * std::sin(coords.y), coords.x * std::cos(coords.y), 0);
}

template<typename Real>
Vector3<Real> pointOnCosineHS(const Real& u, const Real& v) {
    Vector3<Real> p = pointOnCircle<Real>(u, v);
    p.z = Real(sqrt(Real(1) - p.x*p.x-p.y*p.y));
    p.normalise();
    return p;
}

template<typename Real>
Vector3<Real> pointOnHS(const Vector3<Real>& n, const Real& u, const Real& v) {
    Vector3<Real> p = pointOnSphere<Real>(u, v);
    if(p.dot(n) < Real(0.0)) p = -p;
    return p;
}

template<typename Real>
Real square(Real x) { return x * x; }
template<typename Real>
Real fresnelReflect(Real n1, Real n2, Real cos_angle)
{
    Real ior = n2/n1;
    Real F;
    Real sqx = square(ior) + square(cos_angle) - Real(1);
    if (sqx > 0.0){
        Real g = std::sqrt(sqx);
        F = Real(0.5) * (square(g - cos_angle) / square(g + cos_angle));
        F = F * (Real(1) + square(cos_angle * (g + cos_angle) - Real(1)) / square(cos_angle * (g - cos_angle) + Real(1)));

        F = std::min(Real(1), std::max(Real(0), F));
    } else F = 1.0;
    return F;
}

template <typename T> int signum(T val) {
    return (T(0) < val) - (val < T(0));
}

template<typename Real>
Real rdiv(Real a, Real b) {
    if (b == (Real)0)
    {
        //if (a == (Real)0) return (Real)0;
        //else
        return std::numeric_limits<Real>::infinity() * signum(a);
        //return Double.POSITIVE_INFINITY * Math.signum(a);
    }
    else
    {
        if (a == (Real)0) return (Real)0;
        else
        {
            if ((a + b) == a)
                return std::numeric_limits<Real>::infinity() * signum(a) * signum(b);
            else return a / b;
        }
    }
}

template<typename Real>
void solveQuadratic(Real c2, Real c1, Real c0, Real& r0, Real& r1, unsigned char& count)
{
    Real d;
    if (c2 == (Real)0)
        if (c1 == (Real)0)
            count = (Real)0;
        else
        {
            count = 1;
            r0 = rdiv(-c0, c1);
        }
    else
    {
        d = c1 * c1 - 4 * c2 * c0;

        if (d == (Real)0)
        {
            count = 1;
            r0 = rdiv(-c1, 2 * c2);
        }
        else
        {
            if (d > 0)
            {
                count = 2;
                r0 = rdiv(-c1 + (Real)sqrt(d), 2 * c2);
                r1 = rdiv(-c1 - (Real)sqrt(d), 2 * c2);
            }
            else
                count = 0;
        }
    }
}


#endif // MYMATH_H
