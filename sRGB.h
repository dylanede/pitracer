#ifndef SRGB_H
#define SRGB_H
#include <algorithm>
#include "Vector3.h"
#include <math.h>
template<typename Real>
class sRGB : public Vector3<Real> {
private:
    Real convertTo(Real v) {
        if(v <= 0.0031308)
            return 12.92 * v;
        else
            return 1.055 * pow(v, 1.0/2.4) - 0.055;
    }

    Real convertFrom(Real v) {
        if(v <= 0.04045)
            return v / 12.92;
        else
            return pow((v + 0.055) / 1.055, 2.4);
    }

public:

    sRGB() : Vector3<Real>() {}
    sRGB(Real a) : Vector3<Real>(a) {}
    sRGB(Real r, Real g, Real b) : Vector3<Real>(r, g, b) {}

    template<typename T>
    sRGB(const Vector3<T>& v) {
        Vector3<Real>::x = convertTo(v.x);
        Vector3<Real>::y = convertTo(v.y);
        Vector3<Real>::z = convertTo(v.z);
    }

   /* operator Vector3<Real>() {
        return Vector3<Real>(convertFrom(Vector3<Real>::x), convertFrom(Vector3<Real>::y), convertFrom(Vector3<Real>::z));
    }*/
    Vector3<Real> toLinear() {
        return Vector3<Real>(convertFrom(Vector3<Real>::x), convertFrom(Vector3<Real>::y), convertFrom(Vector3<Real>::z));
    }

    template<typename A>
    operator sRGB<A>() {
        return sRGB<A>(Vector3<Real>::x, Vector3<Real>::y, Vector3<Real>::z);
    }

    sRGB<unsigned char> toUChar() {
#ifdef STACCATO
        return sRGB<unsigned char>(
                    (unsigned char)255*round(std::max(std::min(Vector3<Real>::x*(Real)1,(Real)1),(Real)0)),
                    (unsigned char)255*round(std::max(std::min(Vector3<Real>::y*(Real)1,(Real)1),(Real)0)),
                    (unsigned char)255*round(std::max(std::min(Vector3<Real>::z*(Real)1,(Real)1),(Real)0)));
#else
        return sRGB<unsigned char>(
                    (unsigned char)round(std::max(std::min(Vector3<Real>::x*(Real)255,(Real)255),(Real)0)),
                    (unsigned char)round(std::max(std::min(Vector3<Real>::y*(Real)255,(Real)255),(Real)0)),
                    (unsigned char)round(std::max(std::min(Vector3<Real>::z*(Real)255,(Real)255),(Real)0)));
#endif
    }
    template<typename T>
    sRGB<T> toReal() {
        return sRGB<T>(
                    T(Vector3<Real>::x)/T(255),
                    T(Vector3<Real>::y)/T(255),
                    T(Vector3<Real>::z)/T(255));
    }

    sRGB& operator=(const Vector3<Real>& v) {
        Vector3<Real>::x = convertTo(v.x);
        Vector3<Real>::y = convertTo(v.y);
        Vector3<Real>::z = convertTo(v.z);
        return *this;
    }
};
#endif // SRGB_H
