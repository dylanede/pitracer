#ifndef TRACER_H
#define TRACER_H

#include "Ray.h"

template<typename Real, typename Result, typename Location>
class Camera;

template<typename Real, typename Result, typename Location>
class Tracer {
public:
    typedef typename Camera<Real, Result, Location>::WeightedSample WeightedSample;
    virtual WeightedSample sample(const Camera<Real, Result, Location>& camera, const typename Camera<Real, Result, Location>::Film& film) = 0;
    virtual Result trace(const Ray<Real>& ray) = 0;
    virtual Real random() = 0;
    virtual void seed(unsigned long seed) = 0;
    Tracer() {}
    virtual ~Tracer() {}
private:
    Tracer(const Tracer&);
    Tracer& operator=(const Tracer&);
};

#endif // TRACER_H
