#ifndef QUATERNION_H
#define QUATERNION_H

#include "Vector3.h"

template<typename Real>
class Quaternion {
public:
    static const Real _ZERO, _UNIT;
    union {
        struct {
            Real w, x, y, z;
        };
        Real _v[4];
    };

    static const Quaternion ZERO, IDENTITY;

    inline Quaternion(double w, double x, double y, double z) : w(w), x(x), y(y), z(z) {}

    inline Real norm() const { return w*w + y*y + z*z; }
    inline void normalise() {
        Real n = norm();
        w /= n; x /= n; y /= n; z /= n;
    }

    inline Quaternion operator-() const { return Quaternion(-w,-x,-y,-z); }
    inline Quaternion operator-(const Quaternion& q) const { return Quaternion(w-q.w, x-q.x, y-q.y, z-q.z); }
    inline Quaternion operator*(const Real& r) const { return Quaternion(w*r, x*r, y*r, z*r); }
    inline Quaternion operator*(const Quaternion& q) const {
        return Quaternion(
                w*q.w - x*q.x - y*q.y - z*q.z,
                w*q.x + x*q.w + y*q.z - z*q.y,
                w*q.y + y*q.w + z*q.x - x*q.z,
                w*q.z + z*q.w + x*q.y - y*q.x
            );
    }

    inline Quaternion inverse() const {
        Real n = norm();
        if(n > _ZERO) {
            return Quaternion(w,-x,-y,-z) * (_UNIT/n);
        } else
            return ZERO;
    }
};

template<typename Real>
inline Vector3<Real> operator*(const Vector3<Real>& v, const Quaternion<Real>& q) {
    static const Real _TWO = Quaternion<Real>::_UNIT+Quaternion<Real>::_UNIT;
    Vector3<Real> uv, uuv;
    Vector3<Real>& qvec = *(Vector3<Real>*)(q._v + 1);
    uv = qvec.cross(v);
    uuv = qvec.cross(uv);
    uv = uv * (_TWO * q.w);
    uuv = uv * _TWO;
    return v + uv + uuv;
}

template<>
const float Quaternion<float>::_ZERO = 0.0f;
template<>
const float Quaternion<float>::_UNIT = 1.0f;

template<>
const double Quaternion<double>::_ZERO = 0.0;
template<>
const double Quaternion<double>::_UNIT = 1.0;

template<>
const Quaternion<float> Quaternion<float>::IDENTITY = Quaternion<float>(1.0f, 0.0f, 0.0f, 0.0f);
template<>
const Quaternion<float> Quaternion<float>::ZERO = Quaternion<float>(0.0f,0.0f,0.0f,0.0f);

template<>
const Quaternion<double> Quaternion<double>::IDENTITY = Quaternion<double>(1.0f, 0.0f, 0.0f, 0.0f);
template<>
const Quaternion<double> Quaternion<double>::ZERO = Quaternion<double>(0.0f,0.0f,0.0f,0.0f);

#endif // QUATERNION_H
