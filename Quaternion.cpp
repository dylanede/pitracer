#include "Quaternion.h"

template<>
const float Quaternion<float>::_ZERO = 0.0f;
template<>
const float Quaternion<float>::_UNIT = 1.0f;

template<>
const double Quaternion<double>::_ZERO = 0.0;
template<>
const double Quaternion<double>::_UNIT = 1.0;
