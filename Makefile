CXX = $(GCC)g++
PNGINC = lodepng.h
PNGSRC = lodepng.cpp
CMRANDINC = CmRandomNumberGenerator.h
CMRANDSRC = CmRandomNumberGenerator.cpp
TTHREADINC = tinythread.h
TTHREADSRC = tinythread.cpp
INCLUDE = lthread.h Bitmap.h Camera.h Checker.h ConstantEmission.h Dielectric.h Group.h KahanBitmapFilm.h Material.h MetropolisTracer.h mymath.h Node.h Pinhole.h Plane.h Quaternion.h Random.h Ray.h Shape.h ShapeInstance.h Sphere.h sRGB.h SimpleBitmapFilm.h SimpleTracer.h TestMaterial.h TestReflect.h Tracer.h Transform.h Vector2.h Vector3.h
SOURCE = main.cpp #ShapeInstance.cpp

all : $(OUT)
clean :
	rm -f *.o
$(OUT) : $(SOURCE) $(INCLUDE) $(PNGINC) $(PNGSRC) $(CMRANDINC) $(CMRANDSRC) $(TTHREADINC) $(TTHREADSRC)
	$(CXX) $(CXXFLAGS) -std=gnu++0x -o $(OUT) $(SOURCE) $(PNGSRC) $(CMRANDSRC) $(TTHREADSRC) -lpthread
