#ifndef TESTREFLECT_H
#define TESTREFLECT_H

#include "Material.h"
#include "Random.h"
#include "mymath.h"

template<typename Real, typename Result>
class TestReflect : public Material<Real, Result> {
private:

public:
    TestReflect() {
    }

    virtual Result sample(const Ray<Real>& ray, const Vector3<Real>& position, const Vector3<Real>& normal, Tracer<Real, Result>& tracer) {


        Real refcomp = fresnelReflect(Real(1.0), Real(2), -ray.direction.dot(normal));
        if(tracer.random() < refcomp) {
            Vector3<Real> refdir = ray.direction.reflect(normal);
            Ray<Real> refray(position + refdir * EPSILON, refdir);
            Result refsample = tracer.trace(refray);
            return refsample;
        } else {
            Vector3<Real> fdir = pointOnCosineHS<Real>();
            Vector3<Real> X, Y, Z;
            normal.basis(X, Y, Z);
            Vector3<Real> envdir = fdir.x * X + fdir.y * Y + fdir.z * Z;
            Ray<Real> envray(position + envdir * EPSILON, envdir);
            //Real comp = Real(1) - refcomp;
            if(tracer.random() < 0.5)
                return tracer.trace(envray);
            //return envsample;
        }
        //return refsample + envsample * 0.5;
    }

    virtual ~TestReflect() {}
};

#endif // TESTREFLECT_H
