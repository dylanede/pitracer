#include "Vector3.h"

template<>
const Vector3<float> Vector3<float>::ZERO = Vector3<float>(0.0f);
template<>
const Vector3<float> Vector3<float>::UNIT = Vector3<float>(1.0f, 1.0f, 1.0f);
template<>
const Vector3<float> Vector3<float>::UNIT_X = Vector3<float>(1.0f, 0.0f, 0.0f);
template<>
const Vector3<float> Vector3<float>::UNIT_Y = Vector3<float>(0.0f, 1.0f, 0.0f);
template<>
const Vector3<float> Vector3<float>::UNIT_Z = Vector3<float>(0.0f, 0.0f, 1.0f);

template<>
const Vector3<double> Vector3<double>::ZERO = Vector3<double>(0.0);
template<>
const Vector3<double> Vector3<double>::UNIT = Vector3<double>(1.0, 1.0, 1.0);
template<>
const Vector3<double> Vector3<double>::UNIT_X = Vector3<double>(1.0, 0.0, 0.0);
template<>
const Vector3<double> Vector3<double>::UNIT_Y = Vector3<double>(0.0, 1.0, 0.0);
template<>
const Vector3<double> Vector3<double>::UNIT_Z = Vector3<double>(0.0, 0.0, 1.0);
