#ifndef VECTOR2_H
#define VECTOR2_H
#include <math.h>

template<typename Real>
class Vector2 {
public:

    union {
        struct {
            Real x, y;
        };
        Real _v[2];
    };

    inline Vector2() : x(0), y(0) {}
    inline Vector2(Real v) : x(v), y(v) {}
    inline Vector2(Real x, Real y) : x(x), y(y) {}
    inline Vector2(Real* v) : x(v[0]), y(v[1]) {}

    static const Vector2 ZERO, UNIT, UNIT_X, UNIT_Y;

    inline Vector2 operator-(const Vector2& v) const { return Vector2(x-v.x, y-v.y); }
    inline Vector2 operator-(const Real& v) const { return Vector2(x-v, y-v); }
    inline Vector2 operator-() { return Vector2(-x, -y); }
    inline Vector2 operator+(const Vector2& v) const { return Vector2(x+v.x, y+v.y); }
    inline Vector2 operator+(const Real& v) const { return Vector2(x+v, y+v); }
    inline Vector2 operator*(const Vector2& v) const { return Vector2(x*v.x, y*v.y); }
    inline Vector2 operator*(const Real& v) const { return Vector2(x*v, y*v); }
    inline Vector2 operator/(const Vector2& v) const { return Vector2(x/v.x, y/v.y); }
    inline Vector2 operator/(const Real& v) const { return Vector2(x/v, y/v); }

    inline Real dot(const Vector2& v) const { return x*v.x + y*v.y; }

    inline Real squaredLength() const { return dot(*this); }
    inline Real length() const { return sqrt(squaredLength()); }

    inline void normalise() {
        Real l = length();
        x /= l; y /= l;
    }

    inline Vector2 normalisedCopy() const {
        Real l = length();
        return Vector2(x/l, y/l);
    }

    inline Vector2 reflect(const Vector2& N) const { return *this - N * (dot(N) * 2); }

    inline Real distance(const Vector2& v) const { return (*this - v).length(); }

};

template<typename Real>
inline Vector2<Real> operator+(const Real& r, const Vector2<Real>& v) { return Vector2<Real>(r+v.x, r+v.y); }
template<typename Real>
inline Vector2<Real> operator-(const Real& r, const Vector2<Real>& v) { return Vector2<Real>(r-v.x, r-v.y); }
template<typename Real>
inline Vector2<Real> operator*(const Real& r, const Vector2<Real>& v) { return Vector2<Real>(r*v.x, r*v.y); }
template<typename Real>
inline Vector2<Real> operator/(const Real& r, const Vector2<Real>& v) { return Vector2<Real>(r/v.x, r/v.y); }

typedef Vector2<float> Vector2f;
typedef Vector2<double> Vector2d;

#ifdef REAL
typedef Vector2<REAL> Vector2r;
#else
typedef Vector2<float> Vector2r;
#endif

template<>
const Vector2<float> Vector2<float>::ZERO = Vector2<float>(0.0f);
template<>
const Vector2<float> Vector2<float>::UNIT = Vector2<float>(1.0f, 1.0f);
template<>
const Vector2<float> Vector2<float>::UNIT_X = Vector2<float>(1.0f, 0.0f);
template<>
const Vector2<float> Vector2<float>::UNIT_Y = Vector2<float>(0.0f, 1.0f);

template<>
const Vector2<double> Vector2<double>::ZERO = Vector2<double>(0.0);
template<>
const Vector2<double> Vector2<double>::UNIT = Vector2<double>(1.0, 1.0);
template<>
const Vector2<double> Vector2<double>::UNIT_X = Vector2<double>(1.0, 0.0);
template<>
const Vector2<double> Vector2<double>::UNIT_Y = Vector2<double>(0.0, 1.0);

#endif // VECTOR2_H
