#ifndef NODE_H
#define NODE_H

#include "Vector3.h"
#include "Ray.h"
#include "Material.h"

template<typename Real, typename Result, typename Location>
class Node
{
public:
    typedef struct Intersection{
        Vector3<Real> position, normal;
        Material<Real,Result,Location>* material;
        Real d;
        bool hit;
        inline Intersection() : hit(false) {}
        inline Intersection(const Vector3<Real>& position, const Vector3<Real>& normal, const Real& d, Material<Real,Result,Location>* material) : hit(true), position(position), normal(normal), d(d), material(material) {}
    } Intersection;

    const Node* parent;
    inline Node(const Node* parent = 0) : parent(parent) {}

    virtual Intersection intersect(const Ray<Real>& ray) const = 0;
    virtual Intersection intersect(const Ray<Real>& ray, Real distance) const = 0;
    virtual bool intersects(const Ray<Real>& ray) const = 0;
    virtual bool intersects(const Ray<Real>& ray, Real distance) const = 0;

    virtual void modRay(Ray<Real>& ray, Real& distance) const {
        if(parent)
            parent->modRay(ray, distance);
    }

    virtual void modRay(Ray<Real>& ray) const {
        if(parent)
            parent->modRay(ray);
    }
};

#endif // NODE_H
