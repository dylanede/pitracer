#ifndef CAMERA_H
#define CAMERA_H

#include <functional>

#include "Vector3.h"
#include "Vector2.h"
#include "Bitmap.h"
#include "Tracer.h"
template<typename Real, typename Result, typename Location>
class Camera {
public:
    class CameraRay : public Ray<Real> {
    public:
        Location loc;
        inline CameraRay(Vector3<Real> origin, Vector3<Real> direction, Location loc) : Ray<Real>(origin, direction), loc(loc) {}
    };

    typedef struct Sample {
        Location loc;
        Vector3<Real> dir;
        Result value;
        inline Sample(const Location& loc, const Vector3<Real>& dir, const Result& value) : loc(loc), dir(dir), value(value) {}
        inline Sample() {}
    } Sample;
    typedef struct WeightedSample {
        Sample contrib;
        Real w;
        inline WeightedSample() {}
        inline WeightedSample(const Sample& contrib, const Real& weight) : contrib(contrib), w(weight) {}
    } WeightedSample;

    class Film {
    public:
        virtual Real importance(const Sample& s) const = 0;
        virtual void expose(const WeightedSample& s) = 0;
        virtual ~Film() {}
    };

    const Node<Real, Result, Location>* parent;

    inline Camera(const Node<Real, Result, Location>* parent = 0) : parent(parent) {}

    virtual CameraRay generate(Tracer<Real, Result, Location>& tracer) const = 0;
    virtual void filter(WeightedSample& sample) const {}
};

template<typename Real, typename Result, typename Value>
class BitmapFilm : public Camera<Real, Result, Vector2<Real> >::Film {
protected:
    size_t width, height;
public:
    typedef typename Camera<Real, Result, Vector2<Real> >::WeightedSample WeightedSample;
    typedef typename Camera<Real, Result, Vector2<Real> >::Sample Sample;

    BitmapFilm(size_t width, size_t height) : width(width), height(height) {}

    virtual void expose(const WeightedSample& s) = 0;
    virtual Real importance(const Sample& s) const = 0;
    virtual void map(std::function<void (const size_t&, const size_t&, const Value&)> f) const = 0;

    inline const size_t& getWidth() const { return width; }
    inline const size_t& getHeight() const { return height; }

    virtual ~BitmapFilm() {}
};

#endif // CAMERA_H
