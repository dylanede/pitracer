#include <iostream>
#include <sstream>
//#include <pthread.h>
#include "tinythread.h"
#include "lthread.h"
#include "fast_mutex.h"
#include <signal.h>

#include "lodepng.h"
#include "Bitmap.h"
#include "sRGB.h"
#include "Vector3.h"
#include "Quaternion.h"
#include "ShapeInstance.h"
#include "Transform.h"
#include "Group.h"
#include "Pinhole.h"

#include "SimpleBitmapFilm.h"
#include "KahanBitmapFilm.h"
#include "ConstantEmission.h"
#include "TestMaterial.h"
#include "Checker.h"
#include "Dielectric.h"
#include "SimpleTracer.h"
#include "MetropolisTracer.h"
#include "Sphere.h"
#include "Plane.h"
using namespace std;

template<typename Real>
void sRGBdiffuse(Bitmap<Vector3<Real> >& in, Bitmap<sRGB<unsigned char> >& out);
void writeBitmapToPNG(Bitmap<sRGB<unsigned char> >& bitmap, const char* filename);

struct Tracelocal {
    const Pinhole<REAL, Vector3r>* camera;
    Tracer<REAL, Vector3r, Vector2r>* tracer;
    BitmapFilm<REAL, Vector3r, Vector3r >* film;
    tthread::mutex* film_m;
    volatile bool* run;
    size_t pcount;
    volatile size_t* spp;
    tthread::mutex* spp_m;
    tthread::condition_variable* spp_cv;
    volatile size_t scount_residual;
    //pthread_mutex_t* film_mutex;
};

void _dotrace(const Pinhole<REAL, Vector3r >& camera,
              Tracer<REAL, Vector3r, Vector2r>& tracer,
              BitmapFilm<REAL, Vector3r, Vector3r >& film,
              tthread::mutex& film_m,
              volatile bool& run,
              size_t pcount,
              volatile size_t& spp,
              tthread::mutex& spp_m,
              tthread::condition_variable& spp_cv,
              volatile size_t& scount_residual) {
    size_t c = 0;
    size_t lockc = 0;
    film_m.lock();
    while(run) {
        //Pinhole<REAL, Vector3r >::CameraRay ray = camera.generate(tracer);
        //Vector3r result = tracer.trace(ray);
        //Pinhole<REAL, Vector3r >::Sample sample(ray.loc, ray.direction, result);
        //camera.filter(sample);
        Pinhole<REAL, Vector3r>::WeightedSample sample = tracer.sample(camera, film);
        {
            lockc++;
            if(lockc == 20) {
                film_m.unlock();
                tthread::this_thread::yield();
                film_m.lock();
                lockc = 0;
            }
            film.expose(sample);
        }

        c++;
        if(c >= pcount) {
            tthread::lock_guard<tthread::mutex> guard(spp_m);
            spp++;
            c -= pcount;
            spp_cv.notify_all();
        }
    }
    film_m.unlock();
    scount_residual = c;
    tthread::lock_guard<tthread::mutex> guard(spp_m);//imposes memory barriers
}

void dotrace(void* _args) {
    Tracelocal& args = *(Tracelocal*)_args;
    const Pinhole<REAL, Vector3r >& camera = *args.camera;
    Tracer<REAL, Vector3r, Vector2r >& tracer = *args.tracer;
    BitmapFilm<REAL, Vector3r, Vector3r >& film = *args.film;
    tthread::mutex& film_m = *args.film_m;
    volatile bool& run = *args.run;
    size_t pcount = args.pcount;
    volatile size_t& spp = *args.spp;
    tthread::mutex& spp_m = *args.spp_m;
    tthread::condition_variable& spp_cv = *args.spp_cv;
    volatile size_t& scount_residual = args.scount_residual;
    _dotrace(camera, tracer, film, film_m, run, pcount, spp, spp_m, spp_cv, scount_residual);
}

volatile bool run = true;

void sighandler(int sig) {
    run = false;
}

int main(int argc, const char** argv)
{
/*
    size_t t = 1;
    size_t b = 0;
    tthread::thread* thr = lthread::new_thread([&] () {
        std::cout << "I'm in a thread!\n";
        std::cout << "'t' is " << t << std::endl;
        b = 1;
    });
    thr->join();
    std::cout << "'b' is " << b << std::endl;
    delete thr;
    */
    signal(SIGABRT, &sighandler);
    signal(SIGTERM, &sighandler);
    signal(SIGINT, &sighandler);
    if(argc != 5) {
        std::cout << "Usage: PiTracer filename   width height interval\n"
                     " e.g.: PiTracer output.png 1024  768    500\n";
        return 1;
    }
    size_t interval, width, height;
    {
        std::istringstream str(argv[2]);
        if(!(str >> width)) {
            std::cout << "Invalid width value.";
            return 1;
        }
    }
    {
        std::istringstream str(argv[3]);
        if(!(str >> height)) {
            std::cout << "Invalid height value.";
            return 1;
        }
    }
    {
        std::istringstream str(argv[4]);
        if(!(str >> interval)) {
            std::cout << "Invalid interval value.";
            return 1;
        }
    }

    Pinhole<REAL, Vector3r > camera(70.0f*PI/180.0f, REAL(width)/height);

    ConstantEmission<REAL, Vector3r, Vector2r> sky(Vector3r::ZERO);//Vector3r(0.2f, 0.4f, 1.0f) * 1.0f);
    TestMaterial<REAL, Vector3r, Vector2r> sphereMat(Vector3r(1.0f,0.5f,0.5f));
    //Checker<REAL, Vector3r, Vector2r> sphereMat(Vector3r(1.0f,1.0f,1.0f), Vector3r(0.0f, 0.0f, 0.0f));
    Dielectric<REAL, Vector3r, Vector2r> sphere3Mat(1.9, 1.0);
    Checker<REAL, Vector3r, Vector2r> planeMat(Vector3r(1.0f,1.0f,1.0f), Vector3r(0.0f, 0.1f, 0.0f));
    //TestMaterial<REAL, Vector3r, Vector2r> planeMat(Vector3r(1.0f,1.0f,1.0f));
    ConstantEmission<REAL, Vector3r, Vector2r> sphere2Mat(Vector3r(1.0f,1.0f,1.0f) * 40.0f);

    Group<REAL, Vector3r, Vector2r> root;

    Transform<REAL, Vector3r, Vector2r> sphereTrans(Vector3r::UNIT, Quaternion<REAL>::IDENTITY, Vector3r(2,0,8));
    Sphere<REAL> sphere;
    ShapeInstance<REAL, Vector3r, Vector2r> sphereNode(&sphere, &sphereMat, 1);
    sphereTrans.setChild(&sphereNode);
    root.addNode(&sphereTrans);

    Transform<REAL, Vector3r, Vector2r> sphere2Trans(Vector3r::UNIT * 0.5f, Quaternion<REAL>::IDENTITY, Vector3r(0,4,8));
    ShapeInstance<REAL, Vector3r, Vector2r> sphere2Node(&sphere, &sphere2Mat, 1);
    sphere2Trans.setChild(&sphere2Node);
    root.addNode(&sphere2Trans);

    Transform<REAL, Vector3r, Vector2r> sphere3Trans(Vector3r::UNIT * 0.8, Quaternion<REAL>::IDENTITY, Vector3r(-1.5,-0.2,6));
    ShapeInstance<REAL, Vector3r, Vector2r> sphere3Node(&sphere, &sphere3Mat, 1);
    sphere3Trans.setChild(&sphere3Node);
    root.addNode(&sphere3Trans);

    Transform<REAL, Vector3r, Vector2r> planeTrans(Vector3r::UNIT, Quaternion<REAL>::IDENTITY, Vector3r(0,-1,0));
    Plane<REAL> plane;
    ShapeInstance<REAL, Vector3r, Vector2r> planeNode(&plane, &planeMat, 1);
    planeTrans.setChild(&planeNode);
    root.addNode(&planeTrans);

    size_t pcount = width * height;
    size_t count = interval * pcount;
#if 1
    size_t nThreads = tthread::thread::hardware_concurrency();
    std::vector<Tracelocal*> locals;
    std::vector<tthread::thread*> threads;
    volatile size_t scaled_spp = 0;
    tthread::mutex spp_m;
    tthread::condition_variable spp_cv;
    volatile bool threadrun = true;
    for(size_t i = 0; i < nThreads; i++) {
        Tracelocal* args = new Tracelocal();
        args->camera = &camera;
        MetropolisTracer<REAL, Vector3r, Vector2r>* t = new MetropolisTracer<REAL, Vector3r, Vector2r>(&sky, &root);
        //SimpleTracer<REAL, Vector3r, Vector2r>* t = new SimpleTracer<REAL, Vector3r, Vector2r>(&sky, &root);
        t->seed(i * 17 - 1);
        //std::cout << t << std::endl;
        args->tracer = t;
        args->film = new KahanBitmapFilm<REAL, Vector3r, Vector3r>(width, height);
        args->film_m = new tthread::mutex();
        //std::cout << args.film << std::endl;
        args->run = &threadrun;
        args->pcount = pcount / nThreads;
        args->spp = &scaled_spp;
        args->spp_m = &spp_m;
        args->spp_cv = &spp_cv;
        args->scount_residual = 0;
        locals.push_back(args);
        threads.push_back(new tthread::thread(&dotrace, (locals[i])));
    }

    //output image update thread
    volatile bool update_last = false;
    volatile bool update_pending = false;
    tthread::condition_variable update_state_cv;
    tthread::mutex update_state_m;
    tthread::thread* update_thread = lthread::new_temp_thread([&] () {
        bool update_run = true;
        Bitmap<Vector3r> output(width, height);
        while(update_run) {
            {
                tthread::lock_guard<tthread::mutex> guard(update_state_m);
                //either a signal or update_pending being true signals an update to commence
                if(!update_pending)
                    update_state_cv.wait(update_state_m);
                if(update_last) update_run = false;
                update_pending = false;
            }
            //std::cout << "\nWriting to disc..." << std::endl;
            output.map([&](const size_t&, const size_t&, Vector3r& v) {
                v = Vector3r(0.0);
            });
            //add up each film
            for(size_t i = 0; i < nThreads; i++) {
                tthread::lock_guard<tthread::mutex> fguard(*locals[i]->film_m);
                locals[i]->film->map([&](const size_t& x, const size_t&y, const Vector3r& v) {
                    output.value(x, y) += v;
                });
            }
            //average
            output.map([&](const size_t&, const size_t&, Vector3r& v) {
                v /= nThreads;
            });
            //convert to correct format, post-process, then write
            Bitmap< sRGB<unsigned char> > outimage(width, height);
            clamp(output, Vector3r::ZERO, Vector3r::UNIT);
            sRGBdiffuse(output, outimage);
            writeBitmapToPNG(outimage, argv[1]);
            //std::cout << "\nWritten to disc" << std::endl;
        }
    });

    // stats and image update signal loop
    {
        tthread::lock_guard<tthread::mutex> guard(spp_m);
        size_t lastwritteninterval = 0;
        while(run) {
            size_t spp = scaled_spp / nThreads;
            std::cout << "\r           \r";
            std::cout <<spp << " SPP";
            std::cout.flush();
            if(spp - lastwritteninterval >= interval) {
                while(spp - lastwritteninterval >= interval)
                    lastwritteninterval += interval;
                tthread::lock_guard<tthread::mutex> guard(update_state_m);
                update_pending = true;
                update_state_cv.notify_all();
            }
            spp_cv.wait(spp_m);
        }
    }

    threadrun = false;
    for(size_t i = 0; i < nThreads; i++)
        threads[i]->join();

    {
        tthread::lock_guard<tthread::mutex> guard(update_state_m);
        update_pending = true;
        update_last = true;
        update_state_cv.notify_all();
    }

    update_thread->join();

    //clean up
    delete update_thread;
    for(size_t i = 0; i < nThreads; i++) {
        delete threads[i];
        delete locals[i]->tracer;
        delete locals[i]->film;
        delete locals[i]->film_m;
        delete locals[i];
    }

    //calculate and write final accurate SPP to stdout
    size_t spp = scaled_spp / nThreads;
    size_t residual = ((scaled_spp - spp * nThreads) * pcount) / nThreads;
    for(size_t i = 0; i < nThreads; i++) {// not simple sum in case of overflow
        //std::cout << locals[i]->scount_residual << std::endl;
        residual += locals[i]->scount_residual;
        if(residual >= pcount) {
            spp++;
            residual -= pcount;
        }
    }
    //size_t scaled_spp = spp;
    if(residual >= pcount / 2) // rounding
        spp++;

    std::cout << "\r           \r";
    std::cout << spp << " SPP";
    std::cout.flush();

    std::cout << std::endl;
#elif 0
    Traceargs args;
    args.numsamples = count;
    args.camera = &camera;
    args.tracer = &tracer;
    args.film = &film;
    dotrace(&args);

/*
    std::vector<pthread_t> threads;
    std::vector<SimpleBitmapFilm<REAL, Vector3r, Vector3r >* > films;
    //pthread_mutex_t film_mutex = PTHREAD_MUTEX_INITIALIZER;
#define THREADS 4
    for(size_t i = 0; i < THREADS; i++) {
        Traceargs args;
        films.push_back(new SimpleBitmapFilm<REAL, Vector3r, Vector3r >(b.getWidth(), b.getHeight()));
        args.numsamples = count / THREADS;
        args.camera = &camera;
        args.tracer = &tracer;
        args.film = films[i];
        //args.film_mutex = &film_mutex;
        threads.push_back(pthread_t());
        pthread_create(&threads[i], NULL, &dotrace, &args);
    }
    for(size_t i = 0; i < THREADS; i++) {
        pthread_join(threads[i], NULL);
        film.merge(*films[i]);
        delete films[i];
    }
    */
#else
    size_t total = 0;
    size_t spp = 0;
    while(run) {
        for(size_t i = 0; i < count && run; i++, total++) {
            Pinhole<REAL, Vector3r >::CameraRay ray = camera.generate(tracer);
            Vector3r result = tracer.trace(ray);
            Pinhole<REAL, Vector3r >::Sample sample(ray.loc, ray.direction, result);
            camera.filter(sample);
            film.expose(sample);
            size_t newspp = total / pcount;//size_t(i*100.0f/count);

            if(newspp != spp) {
                spp = newspp;
                std::cout << "           \r";
                std::cout << spp << " SPP";
                std::cout.flush();
            }

        }
        size_t newspp = total / pcount;//size_t(i*100.0f/count);

        if(newspp != spp) {
            spp = newspp;
            std::cout << "           \r";
            std::cout << spp << " SPP";
            std::cout.flush();
        }
        //std::cout << std::endl;

        Bitmap< Vector3r > b(width, height);
        Bitmap< sRGB<unsigned char> > outimage(width, height);
        film.writeResult(b);
        clamp(b, Vector3r::ZERO, Vector3r::UNIT);
        sRGBdiffuse(b, outimage);
        //Bitmap<sRGB<REAL> > out((Bitmap<sRGB<REAL> >)b);

        writeBitmapToPNG(outimage, argv[1]);
    }
    std::cout << std::endl;
#endif
    return 0;
}

//alters input, be warned
template<typename Real>
void sRGBdiffuse(Bitmap<Vector3<Real> >& in, Bitmap<sRGB<unsigned char> >& out) {
    size_t w = in.getWidth(), h = in.getHeight();
    size_t wm1 = w - 1, hm1 = h - 1;
    bool d = false;
    for(size_t y = 0; y < h; y++) {
        if(d)
            for(size_t x = 0; x < w; ++x) {
                Vector3<Real>& value = in.value(x, y);
                sRGB<Real> gvalue(value);
                sRGB<unsigned char> outvalue = gvalue.toUChar();
                out.value(x, y) = outvalue;
                Vector3<Real> rounded (outvalue.toReal<Real>().toLinear());
                Vector3<Real> error = value - rounded;
                //distribute
                if(x != wm1)
                    if(y != hm1) {
                        if(x != 0) {
                            in.value(x+1, y) += Real(7)/Real(16) * error;
                            in.value(x-1, y+1) += Real(3)/Real(16) * error;
                            in.value(x, y+1) += Real(5)/Real(16) * error;
                            in.value(x+1, y+1) += Real(1)/Real(16) * error;
                        } else {
                            in.value(x+1, y) += Real(7)/Real(13) * error;
                            //in.value(x-1, y+1) += Real(3)/Real(16) * error;
                            in.value(x, y+1) += Real(5)/Real(13) * error;
                            in.value(x+1, y+1) += Real(1)/Real(13) * error;
                        }
                    } else
                        in.value(x+1, y) += error;
                else {
                    if(y != hm1) {
                        if(x != 0) {
                            in.value(x-1, y+1) += Real(6)/Real(16) * error;
                            in.value(x, y+1) += Real(10)/Real(16) * error;
                        } else
                            in.value(x, y+1) += error;
                    }
                }
            }
        else
            for(int x = w-1; x >= 0; --x) {
                Vector3<Real>& value = in.value(x, y);
                sRGB<Real> gvalue(value);
                sRGB<unsigned char> outvalue = gvalue.toUChar();
                out.value(x, y) = outvalue;
                Vector3<Real> rounded (outvalue.toReal<Real>().toLinear());
                Vector3<Real> error = value - rounded;
                //distribute
                if(x != 0)
                    if(y != hm1) {
                        if(x != wm1) {
                            in.value(x-1, y) += Real(7)/Real(16) * error;
                            in.value(x+1, y+1) += Real(3)/Real(16) * error;
                            in.value(x, y+1) += Real(5)/Real(16) * error;
                            in.value(x-1, y+1) += Real(1)/Real(16) * error;
                        } else {
                            in.value(x-1, y) += Real(7)/Real(13) * error;
                            //in.value(x-1, y+1) += Real(3)/Real(16) * error;
                            in.value(x, y+1) += Real(5)/Real(13) * error;
                            in.value(x-1, y+1) += Real(1)/Real(13) * error;
                        }
                    } else
                        in.value(x-1, y) += error;
                else {
                    if(y != hm1) {
                        if(x != wm1) {
                            in.value(x+1, y+1) += Real(6)/Real(16) * error;
                            in.value(x, y+1) += Real(10)/Real(16) * error;
                        } else
                            in.value(x, y+1) += error;
                    }
                }
            }
    }
}

void writeBitmapToPNG(Bitmap<sRGB<unsigned char> >& bitmap, const char *filename) {
    std::vector<unsigned char> image;
    size_t count = bitmap.getWidth() * bitmap.getHeight();
    image.resize(count * 4);
    for(size_t i = 0, j = 0; i < count; i++, j+=4) {
        sRGB<unsigned char>& c = bitmap.values[i];//((sRGB<REAL>)bitmap.values[i]).toUChar();
        image[j] = c.x;
        image[j+1] = c.y;
        image[j+2] = c.z;
        image[j+3] = 255;
    }
    lodepng::encode(filename, image, bitmap.getWidth(), bitmap.getHeight());
}
