#ifndef TESTMATERIAL_H
#define TESTMATERIAL_H
#include "Material.h"
#include "mymath.h"
template<typename Real, typename Result, typename Location>
class TestMaterial : public Material<Real, Result, Location> {
private:
    Result svalue;
    Real lum;
public:
    TestMaterial(Result value){
        lum = pow(luminance(value),1.0 / 2.2);
        svalue = value;// / lum;
        //lum *= 0.5;// div by lambert integral
    }

    virtual Result sample(const Ray<Real>& ray, const Vector3<Real>& position, const Vector3<Real>& normal, Tracer<Real, Result, Location>& tracer) const {
        //return lerp(clamp(normal.dot(Vector3<Real>::UNIT_Y), (Real)0.0, (Real)1.0), value, Vector3<Real>::UNIT_Y);
        //Vector3<Real> fake = lerp(clamp(normal.dot(Vector3<Real>::UNIT_Y), (Real)0.0, (Real)1.0), Vector3<Real>::ZERO, value);
        Vector3<Real> fdir = pointOnCosineHS<Real>(tracer.random(), tracer.random());
        Vector3<Real> X, Y, Z;
        normal.basis(X, Y, Z);
        Vector3<Real> envdir = fdir.x * X + fdir.y * Y + fdir.z * Z;
        envdir.normalise();
        Ray<Real> envray(position + envdir * EPSILON, envdir);
        //if(tracer.random() < lum)
            return svalue * tracer.trace(envray);
        //else
        //    return Result(0.0);
        //Result envsample = tracer.trace(envray, lum * 0.5);
        //return envsample * svalue;
        //return value;
    }

    virtual ~TestMaterial() {}
};

#endif // TESTMATERIAL_H
